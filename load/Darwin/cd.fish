# Wrappers around cd(1)

function cdd
  cd ~/Desktop $argv
end

function cdo
  cd ~/Documents $argv
end

function cdp
  cd ~/Documents/Projekte $argv
end

function cdw
  cd ~/Downloads $argv
end
