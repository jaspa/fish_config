
function term -d 'open new terminal'
    if test (count $argv) -eq 0
        open -a Terminal .
    else
        for dir in $argv
            open -a Terminal "$dir"
        end
    end
end

