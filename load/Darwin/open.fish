# Wrappers around open(1)

function op
  open $argv
end

function op.
  open . $argv
end
