# Wrapper around ls(1)

set default '-FG'
set all     '-A'
set list    '-lh'

function ls
  command ls $default $argv
end

function l
  ls $argv
end

# LIST ALL

function la
  ls $all $argv
end

function a
  ls $all $argv
end

# LIST IN A LIST

function ll
  ls $list $argv
end

function lla
  ls $list $all $argv
end
