
function mdcd -d 'create dir and change into'
    for dir in $argv
        mkdir -p $dir
    end
    cd "$argv[-1]"
end

