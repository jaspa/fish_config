function dec --description 'decrease the given variable by one' --no-scope-shadowing
	for var in $argv
		[ $var != -p ]
		and eval "set $var (math \$$var-1)"
		[ x$var != x-p -a x$argv[1] = x-p ]
		and echo $$var
	end
end
