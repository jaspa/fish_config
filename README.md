# README

My simple [*fish*](https://fishshell.com) configuration.

## Installation

*fish* expects its main configuration file `config.fish` at `~/.config/fish/config.fish`.

Clone this repository into `~/.config/fish` and you're all set up:

```shell
$ git clone <repository-url> ~/.config/fish
```