set FISH_CONFIG_DIR ~/.config/fish

# Put your modifications to $PATH into here.
set -l PATH_FILE $FISH_CONFIG_DIR/load/_path.fish

test -e $PATH_FILE
  and source $PATH_FILE

for script in $FISH_CONFIG_DIR/load/*.fish
  test $script != $PATH_FILE
    and source $script
end

# Source OS specific helpers
for script in $FISH_CONFIG_DIR/load/(uname)/*.fish
  test $script != "$PATH_FILE"
    and source $script
end

set __fish_git_prompt_showcolorhints 1

if type boot2docker >/dev/null ^&1
  boot2docker shellinit ^/dev/null | \
    perl -ne 'if (/export (.+?)=(.+)$/) { print "set -x \'$1\' \'$2\'\\n" }' | \
    source
end
